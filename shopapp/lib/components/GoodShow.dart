import 'package:flutter/material.dart';

class GoodShowPage extends StatefulWidget {
  final String title;
  final String goodUrl;
  final int id;

// GoosdShowPage ({Key?key,required this.title,required this.goodUrl,required this.id} ) : super:(Key: key);
  GoodShowPage(
      {Key? key, required this.title, required this.goodUrl, required this.id})
      : super(key: key);

  @override
  _GoodShowPageState createState() => _GoodShowPageState();
}

class _GoodShowPageState extends State<GoodShowPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        //将元素包起来添加可点击事件
        child: Column(
          //元素垂直布局
          children: [
            //元素内容
            Image.network(widget.goodUrl, //动态的数据（src使用组件传的内容）
                fit: BoxFit.cover //图片满屏
                ),
            Text(
              widget.title, //文本内容
              maxLines: 1, //最大两行
              overflow: TextOverflow.ellipsis, //超出省略
            ),
            SizedBox(
              height: 10,
            ), //盒子高度
            Row(
              //底部内容 （水平分布）
              children: [
                Expanded(
                  child: Text(
                    '自营',
                    style: TextStyle(
                      color: Colors.white,
                      backgroundColor: Colors.red,
                      letterSpacing: 1.0,
                    ),
                  ),
                  flex: 1,
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: Text(
                  '￥33.9',
                  textDirection: TextDirection.rtl,
                  style: TextStyle(color: Colors.red),
                ))
              ],
            )
          ],
        ),
        onTap: () {
          print('点击了某个商品：${widget.id}');
          //  Navigator.pushNamed(context, "/goodDetail",arguments:widget.id);
          Navigator.pushNamed(context, '/goodDetail',
              arguments: widget.id); //页面跳转并传值
        },
      ),
    );
  }
}
