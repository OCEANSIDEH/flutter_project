import 'package:flutter/material.dart';
import 'GoodShow.dart';

class GoodListPage extends StatefulWidget {
  final List goods;
  final String title;
  final IconData incos;

  GoodListPage(
      {Key? key, required this.goods, required this.title, required this.incos})
      : super(key: key);

  @override
  _GoodListPageState createState() => _GoodListPageState();
}

class _GoodListPageState extends State<GoodListPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10), //盒子内边距，左上右下
          decoration: BoxDecoration(color: Colors.blue), //元素的背景色
          child: Row(
            //水平布局
            children: [
              Expanded(
                //左边占位符 （这样元素可均匀分配）
                child: Text(""),
                flex: 1,
              ),
              Icon(widget.incos),
              Text(
                '${widget.title}', //通过使用组件的地方传过来的值（widget.参数名）
                style: TextStyle(color: Colors.white),
              ),
              Expanded(
                //右边边占位符
                child: Text(""),
                flex: 1,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Column(
            //元素垂直分布
            children: [
              //每个子元素
              Row(
                //水平分布 (两个)
                children: [
                  //每一个元素
                  Expanded(
                    child: GoodShowPage(
                      goodUrl:
                          'http://img30.360buyimg.com/sku/jfs/t1/122554/32/8357/377664/5f1ffa98E951b2cd7/c89b649fcf9923a5.jpg',
                      title:
                          '努比亚 nubia 红魔5S 12GB+256GB 冰封银翼 电竞游戏手机 144Hz屏幕刷新率 内置风扇散热',
                      id: 1,
                    ),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10,
                    height: 20,
                  ),
                  Expanded(
                    child: GoodShowPage(
                      goodUrl:
                          'http://img30.360buyimg.com/sku/jfs/t1/122554/32/8357/377664/5f1ffa98E951b2cd7/c89b649fcf9923a5.jpg',
                      title:
                          '努比亚 nubia 红魔5S 12GB+256GB 冰封银翼 电竞游戏手机 144Hz屏幕刷新率 内置风扇散热',
                      id: 2,
                    ),
                    flex: 1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
              Row(
                //水平分布
                children: [
                  //每一个元素
                  Expanded(
                    child: GoodShowPage(
                      goodUrl:
                          'http://img30.360buyimg.com/sku/jfs/t1/122554/32/8357/377664/5f1ffa98E951b2cd7/c89b649fcf9923a5.jpg',
                      title:
                          '努比亚 nubia 红魔5S 12GB+256GB 冰封银翼 电竞游戏手机 144Hz屏幕刷新率 内置风扇散热',
                      id: 3,
                    ),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10,
                    height: 20,
                  ),
                  Expanded(
                    child: GoodShowPage(
                      goodUrl:
                          'http://img30.360buyimg.com/sku/jfs/t1/122554/32/8357/377664/5f1ffa98E951b2cd7/c89b649fcf9923a5.jpg',
                      title:
                          '努比亚 nubia 红魔5S 12GB+256GB 冰封银翼 电竞游戏手机 144Hz屏幕刷新率 内置风扇散热',
                      id: 4,
                    ),
                    flex: 1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
