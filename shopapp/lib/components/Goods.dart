import 'package:flutter/material.dart';
import 'GoodShow.dart';

class GoodsPage extends StatefulWidget {
  final List goods;
  final String title;
  final IconData incos;

  GoodsPage(
      {Key? key, required this.goods, required this.title, required this.incos})
      : super(key: key);

  @override
  _GoodsPageState createState() => _GoodsPageState();
}

class _GoodsPageState extends State<GoodsPage> {
  // 把组件提出来 （返回return的是什么就与什么同级）
  Widget _goodsInfo() {
    List<Widget> mGoodsCard = []; //定义循环的数组
    Widget content;
    for (int i = 0; i < 4; i++) {
      mGoodsCard.add(InkWell(
        //循环向数组添加元素
        onTap: () => print('i${i}'), //点击事件
        child: Text('你好啊'),
      ));
    }
    content = Column(
      children: mGoodsCard, //变量布局
    );
    return content; //返回
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Container(),
        _goodsInfo(),
      ],
    );
  }
}
