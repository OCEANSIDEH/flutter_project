// @dart=2.9
import 'package:flutter/material.dart';
//底部导航
import 'page/toolbar/Toolbar.dart';
import 'routers/AppRouter.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          appBar: new AppBar(
            // backgroundColor: Color., //设置appbar背景颜色
            centerTitle: true, //设置标题是否局中
          ),
          bottomNavigationBar: Toolbar(),
        ),
        initialRoute: '/',
        onGenerateRoute: AppRouter().getRoutes);
  }
}
