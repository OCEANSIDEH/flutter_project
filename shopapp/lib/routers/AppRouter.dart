import 'package:flutter/material.dart';

// 引入跳转的页面
import '../page/shop/GoodDetail.dart';

class AppRouter {
  //路由名字的配置
  static final _routes = {
    '/goodDetail': (BuildContext content, {required int args}) =>
        GoodDetailPage(id: args), //所传的参数
  };

  /// 监听route
  Route? getRoutes(RouteSettings settings) {
    String? routeName = settings.name;
    final Function builder = AppRouter._routes[routeName] as Function;
    print(settings);

    if (builder == null) {
      return null;
    } else {
      return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) =>
              builder(context, args: settings.arguments));
    }
  }
}
