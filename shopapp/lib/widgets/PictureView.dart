//图片查看
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:extended_image/extended_image.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:shopapp/styles/CustomSize.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:dio/dio.dart';
// 一定要引入这三个插件
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:ui' as ui;

// @FFRoute(
//   name: 'picture_dialog',
//   routeName: 'SlidePageItem',
//   description: 'Simple demo for Sliding.',
//   pageRouteType: PageRouteType.transparent,
// )
class PictureDialog extends StatefulWidget {
  var arguments;
  PictureDialog({this.arguments});

  @override
  _PictureDialog createState() => _PictureDialog();
}

class _PictureDialog extends State<PictureDialog> {
  var image; //显示的图片
  var imageList = []; //图片数组
  int index = 1; //当前图片数组的索引
  var width = Screen.screenWidth; //手机屏幕的宽度
  var _scrollController; //滚动控制器
  var down = 0.0; //触开始的时候的left
  var half = false; //是否超过一半
  var drag = false; //是否是拖拽

  @override
  void initState() {
    super.initState();
    setState(() {});
    print(widget.arguments['img']);
    this.image = widget.arguments['img'];
    //如果只有一张图片
    if (widget.arguments['imgliset'] == null) {
      this.imageList.add(this.image);
    } else {
      //有数组
      this.imageList = widget.arguments['imgliset'];
      localImage(); //定位某张图
    }
    this.width = Screen.screenWidth;
    print(this.imageList);
  }

  //定位数组的某一张图片 并滚动到该数组的某一张
  localImage() {
    var n = 0;
    var nowIdx; //当前图片第几张
    this.imageList.forEach((i) {
      print('i' + i);
      n = n + 1; //当前的索引+1
      if (i == this.image) {
        nowIdx = n; //第几张
      }
    });
    print(nowIdx);
    if (nowIdx > 1) {
      //第二张以后的

      print(this.width);
      _scrollController = ScrollController(
          //滑动控件
          //应该为当前的左边全部 当前为第二张，前面已经滚动为一张
          initialScrollOffset: this.width * (nowIdx - 1) //屏幕宽度乘以数量
          );

      print(this.width * (nowIdx - 1));
    } else {
      //没有移动
      _scrollController = ScrollController(
        initialScrollOffset: 0,
      );
    }
    setState(() {
      //更换索引和图片
      image = this.image;
      index = nowIdx;
    });
  }

  //关闭
  deleteImage() {
    Navigator.pop(context);
  }

  //滚动事件
  _scrollC(index, w) {
    //滚动值和屏幕宽度
    _scrollController.animateTo(
      (index - 1) * w,
      duration: Duration(milliseconds: 200),
      curve: Curves.easeIn,
    );
  }

  //下一张 （更换每一个宽度去滚动）
  nextImage(w) {
    setState(() {
      index = index + 1;
      _scrollC(index, w);
    });
  }

  //上一张
  lastImage(w) {
    setState(() {
      index = index - 1;
      _scrollC(index, w);
    });
  }

  //水平滑动时 到
  moveEnd(e, w, l) {
    var end = e.primaryVelocity;
    if (end > 10 && index > 1) {
      lastImage(w);
    } else if (end < -10 && index < l) {
      nextImage(w);
    } else if (half == true) {
      //图片滚动超过一半可换下一张
      if (down > w / 2 && index < l) {
        //右边开始滑动超过一半,则下一张
        nextImage(w);
      } else if (down < w / 2 && index > 1) {
        lastImage(w);
      } else {
        _scrollC(index, w); //滚动
      }
    } else {
      _scrollC(index, w);
    }
  }

  //图片可滑动 （图片滑动到一半时）
  imgMove(e, w, l) {
    //down 为起点
    var left = e.position.dx;
    var now = (index - 1) * w;
    var move = left - down; //移动距离
    if (left - down > w / 2 || down - left > w / 2) {
      half = true;
    } else {
      half = false;
    }
    _scrollController.jumpTo(now - move);
  }

  //保存图片
  // void saveImage(globalKey) async {
  //   print('this.image'+this.image);
  //   var response = await Dio().get(this.image, options: Options(responseType: ResponseType.bytes));
  //   // print('response$response');
  //   final result = await ImageGallerySaver.saveImage(Uint8List.fromList(response.data));
  //   // ByteData bytes = await rootBundle.load(this.image);
  //   // final result = await ImageGallerySaver.saveImage(bytes.buffer.asUint8List()); //这个是核心的保存图片的插件
  //   print('result:$result');
  //   if(result!= null){
  //     print('保存成功');
  //   }else{
  //     print('保存失败');
  //   }
  // }

  //保存图片方法
  /// 保存图片
  static Future<void> saveImage(GlobalKey globalKey) async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext.findRenderObject();
    var image = await boundary.toImage(pixelRatio: 6.0);
    // var ui;
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    // final result = await ImageGallerySaver.saveImage(pngBytes,
    //     quality: 60, name: "hello");
    // if (result) {
    //   print('ok');
    //   // toast("保存成功", wring: false);
    // } else {
    //   print('error');
    // }

    if (Platform.isIOS) {
      var status = await Permission.photos.status;
      if (status.isUndetermined) {
        Map<Permission, PermissionStatus> statuses = await [
          Permission.photos,
        ].request();
        saveImage(globalKey);
      }
      if (status.isGranted) {
        final result = await ImageGallerySaver.saveImage(pngBytes,
            quality: 60, name: "hello");
        if (result) {
          print('ok');
          // toast("保存成功", wring: false);
        } else {
          print('error');
          // toast("保存失败");
        }
      }
      if (status.isDenied) {
        print("IOS拒绝");
      }
    } else if (Platform.isAndroid) {
      var status = await Permission.storage.status;
      if (status.isUndetermined) {
        Map<Permission, PermissionStatus> statuses = await [
          Permission.storage,
        ].request();
        saveImage(globalKey);
      }
      if (status.isGranted) {
        print("Android已授权");
        final result = await ImageGallerySaver.saveImage(pngBytes,
            quality: 60, name: 'iii');
        if (result != null) {
          print('ok');
          // toast("保存成功", wring: false);
        } else {
          print('error');
          // toast("保存失败");
        }
      }
      if (status.isDenied) {
        print("Android拒绝");
      }
    }
  }

  Widget list(w, l, globalKey) {
    List<Widget> array = [];
    this.imageList.forEach((i) {
      var gestureKey;
      array.add(
        Listener(
          onPointerMove: (e) {
            //手指按下的距离
            imgMove(e, w, l);
          },
          onPointerDown: (e) {
            down = e.position.dx;
          },
          child: ExtendedImageSlidePage(
            //图片拖动可移动退出
            // slideAxis: SlideAxis.both,
            // slideType: SlideType.onlyImage,
            onSlidingPage: (state) {
              //var offset= state.offset;
              var showSwiper = !state.isSliding;
              // print('showSwiper$showSwiper');
              // print(state.isSliding);
              if (state.isSliding) {
                drag = true;
              } else {
                drag = false;
              }
              print('half$half');
            },
            child: GestureDetector(
                onTap: () {
                  //点击关闭
                  deleteImage();
                },
                onLongPress: () {
                  print('长按');
                  print('globalKey$globalKey');
                  saveImage(globalKey);
                },
                onHorizontalDragEnd: (e) {
                  //水平滑动
                  print('e$e');
                  moveEnd(e, w, l);
                },
                child: Container(
                  //图片控件
                  width: w,
                  color: Colors.black,
                  // margin: EdgeInsets.only(top: 40),
                  // child: i != null ? ExtendedImage.network(i) : Image.file(i),
                  child: i != null && i.toString().startsWith('http')
                      ? ExtendedImage.network(
                          i,
                          width: w,
                          fit: BoxFit.contain,
                          enableSlideOutPage: true, //是否开启滑动退出页面效果
                          mode: ExtendedImageMode.gesture, //可缩放
                        )
                      : i != null
                          ? ExtendedImage.file(
                              File(i),
                              width: w,
                              fit: BoxFit.contain,
                              enableSlideOutPage: true, //是否开启滑动退出页面效果
                              mode: ExtendedImageMode.gesture, //可缩放
                            )
                          : Container(),
                )),
          ),
        ),
      );
    });
    // return Column(
    //   children: array,
    // );
    return ListView(
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      children: array,
    );
  }

  @override
  Widget build(BuildContext context) {
    var w = this.width; //屏幕宽度
    var l = this.imageList.length; //图片数组的数量
    GlobalKey globalKey = GlobalKey();
    // return RepaintBoundary(
    //   key: globalKey,
    return Container(
      color: Colors.black,
      child: Stack(
        children: <Widget>[
          list(w, l, globalKey), //图片显示
          // Positioned(  //数量的变化
          //   top: 40,
          //   child: Container(
          //       alignment: Alignment.center,
          //       width: w,
          //       child: Text('$index/$l',style: TextStyle(
          //         decoration: TextDecoration.none,
          //         color: Colors.white,fontSize: 16,
          //         fontWeight: FontWeight.w400,
          //       ))
          //   ),
          // ),
          // Positioned(  //关闭按钮
          //   top: 40,
          //   right: 30,
          //   child: Container(
          //     alignment: Alignment.centerLeft,
          //     width: 20,
          //     child: GestureDetector(
          //       onTap: () {deleteImage();},  //点击关闭
          //       child: Icon(Icons.close,color: Colors.white,),
          //     ),
          //   ),
          // ),
        ],
      ),
      // )
    );
  }
}
