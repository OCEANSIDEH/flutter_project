import 'package:flutter/material.dart';

// 引入底部导航页面
import '../home/Home.dart';
import '../shop/Shop.dart';
import '../my/My.dart';

class Toolbar extends StatefulWidget {
  Toolbar({Key? key}) : super(key: key);

  @override
  _ToolbarState createState() => _ToolbarState();
}

class _ToolbarState extends State<Toolbar> {
  // 导航页面
  List _page = [new HomePage(), new ShopPage(), new MyPage()];

  // 点击的索引
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var page = this._page[_currentIndex]; //当前页面
    return Container(
      child: Scaffold(
        body: Container(
          // color: hex(),
          child: page,
        ),
        bottomNavigationBar: Theme(
          data: ThemeData(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent),
          child: BottomNavigationBar(
            items: [
              // BottomNavigationBarItem(
              //     icon: Image.asset('images/home.jpg'), label: '首页'),
              BottomNavigationBarItem(icon: Icon(Icons.category), label: '首页'),
              BottomNavigationBarItem(icon: Icon(Icons.category), label: '商品'),
              BottomNavigationBarItem(icon: Icon(Icons.person), label: '我的'),
            ],
            currentIndex: _currentIndex,
            onTap: (index) {
              setState(() {
                //动态更换数据
                this._currentIndex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}
