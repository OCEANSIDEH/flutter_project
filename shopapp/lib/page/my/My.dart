import 'package:flutter/material.dart';

class MyPage extends StatefulWidget {
  // 可声明变量
  MyPage({Key? key}) : super(key: key);

// 将创建的组件添加进状态组件里面（这样才能获取动态的数据）
  @override
  _MyPageState createState() => _MyPageState();
}

// 页面所要展示的内容
class _MyPageState extends State<MyPage> {
  @override
  void initState() {
    // 获取动态的数据
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //头部内容
        centerTitle: true, //是否居中
        title: Text("我的"),
      ),
      body: SingleChildScrollView(),
    );
  }
}
