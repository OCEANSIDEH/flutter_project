import 'package:flutter/material.dart';

class GoodDetailPage extends StatefulWidget {
  // 接收传过来的参数
  final int id;

  GoodDetailPage({Key? key, required this.id}) : super(key: key);

  @override
  _GoodDetailPageState createState() => _GoodDetailPageState();
}

class _GoodDetailPageState extends State<GoodDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('商品详情'),
        centerTitle: true,
      ), //页面顶部标题
      body: Container(
        child: Text('接收的参数ID为：${widget.id}'),
      ),
    );
  }
}
