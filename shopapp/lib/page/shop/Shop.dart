import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopapp/widgets/PictureView.dart';

class ShopPage extends StatefulWidget {
  // 可声明变量
  ShopPage({Key? key}) : super(key: key);

// 将创建的组件添加进状态组件里面（这样才能获取动态的数据）
  @override
  _ShopPageState createState() => _ShopPageState();
}

// 页面所要展示的内容
class _ShopPageState extends State<ShopPage> {
  @override
  void initState() {
    // 获取动态的数据
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          //头部内容
          centerTitle: true, //是否居中
          title: Text("商品ee"),
        ),
        body: SingleChildScrollView(
            child: Column(children: [
          Container(
              padding: EdgeInsets.all(10),
              height: 200,
              // child: Image.asset('images/pict_test.jpg'),
              child: InkWell(
                  onTap: () {
                    print(333);
                    Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => PictureDialog(arguments: {
                                  'img': 'images/pict_test.jpg',
                                  'imgliset': ['images/pict_test.jpg']
                                })));
                  },
                  child: Image(
                      image: AssetImage("images/pict_test.jpg"),
                      fit: BoxFit.fill)))
        ])));
  }
}
