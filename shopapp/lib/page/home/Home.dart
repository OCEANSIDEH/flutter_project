import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../../dao/service_method.dart';
import '../../components/GoodList.dart';
import '../../components/Goods.dart';

class HomePage extends StatefulWidget {
  // 可声明变量
  HomePage({Key? key}) : super(key: key);
// 将创建的组件添加进状态组件里面（这样才能获取动态的数据）
  @override
  _HomePageState createState() => _HomePageState();
}

// 页面所要展示的内容
class _HomePageState extends State<HomePage> {
  // String homePageContent = '正在获取数据';
  List banners = []; //轮播图

  @override
  void initState() {
    getHomePageContent().then((val) {
      List<dynamic> data = val['data']; //获取返回的val里面的data数据val['data']
      setState(() {
        data.forEach((v) {
          print('v' + v);
          //数组循环添加
          this.banners.add(v['image']);
        });
        print(this.banners);
      });
    });

    // 获取动态的数据
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //头部内容
        centerTitle: true, //是否居中
        title: Text("首页"),
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        Container(
            padding: EdgeInsets.all(10),
            height: 200,
            child: Swiper(
              itemCount: this.banners.length,
              itemBuilder: (context, index) {
                return Image.network(this.banners[index], fit: BoxFit.fill);
              },
              pagination: SwiperPagination(),
              // control: SwiperControl(),
              autoplay: true,
            )),
        Container(
            child:
                GoodListPage(title: "热销商品", incos: Icons.hot_tub, goods: [])),
        Container(
            child: GoodListPage(
                title: "精品推荐",
                incos: Icons.access_time_filled_rounded,
                goods: [])),
        Container(
          child: GoodsPage(
              title: "测试组件",
              incos: Icons.access_time_filled_rounded,
              goods: []),
        )
      ])),
    );
  }
}
