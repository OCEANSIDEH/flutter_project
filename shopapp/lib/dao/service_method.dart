// ignore: import_of_legacy_library_into_null_safe
import "package:dio/dio.dart";
import 'dart:async';
import 'dart:io';
import '../config/service_url.dart';

Future getHomePageContent() async {
  try {
    print('开始获取首页数据...............');
    Response response;
    Dio dio = new Dio();
    // dio.options.contentType =
    //     ContentType.parse("application/x-www-form-urlencoded") as String;
    response = await dio.get(servicePath['indexcarouselmap']);
    if (response.statusCode == 200) {
      return response.data; //将请求的数据返回出去
    } else {
      throw Exception('后端接口出现异常，请检测代码和服务器情况.........');
    }
  } catch (e) {
    return print('ERROR:======>${e}');
  }
}
